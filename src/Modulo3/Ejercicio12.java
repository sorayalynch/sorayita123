package Modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese numero");
		Scanner num = new Scanner(System.in);
		int var=num.nextInt();
		if (var>0 && var<=12) {
			System.out.println("Primera docena");
		}
		if (var>13 && var<=24) {
			System.out.println("Segunda docena");
		} 
		if (var>25 && var<=36) {
			System.out.println("Tercera docena");
		}
		if (var<0 || var>36) {
			System.out.println("Fuera de rango");
		}

	}

}
