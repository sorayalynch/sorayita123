package Modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				byte 		bmin = -128;
				byte 		bmax = 127;
//				reemplazar el 0 por el valor que corresponda en todos los caso
				short 	smin = -32768;
				short 	smax = 32767;
				int 		imax = -2147483648;
				int 		imin = 2147483647;
				long 		lmin = (long)-1-(long)Math.pow(2 , 63);;
				long 		lmax = (long)Math.pow(2 , 63);;
				System.out.println("tipo\tminimo\tmaximo");
				System.out.println("....\t......\t......");
				System.out.println("\nbyte\t" + bmin + "\t" + bmax);
				System.out.println("\nshort\t" + smin + "\t" + smax);
				System.out.println("\nint\t" + imin + "\t" + imax);
				System.out.println("\nlong\t" + lmin + "\t" + lmax); 
				System.out.println("\n\"n\"bits\t-2^(n-1)\t\t2^(n-1)-1");
				
	}}
		
		